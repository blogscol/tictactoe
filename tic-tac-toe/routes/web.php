<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('menu');
});


Route::get('/nueva-partida', function () {
    return view('nueva-partida');
});

Route::post('/new-room/','RoomController@newRoom')->name('new-room');

Route::get('/init-room/{roomID}','RoomController@initRoom');

Route::post('/close-room/','RoomController@closeRoom')->name('close-room');

Route::get('/unirse-partida', function () {
    return view('unirse-partida');
});

Route::post('/enter-room/','RoomController@enterRoom')->name('enter-room');