<?php

namespace App\Http\Controllers;

use App\Room;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    //Crea una nueva sala de juego
    public function newRoom(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'player1' => 'required|string|min:1|max:80',
            'player2' => 'required|string|min:1|max:80'
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)
                        ->withInput();
        }
        else
        {
            $room = new Room([
                'player1' => $request->get('player1'),
                'player2' => $request->get('player2'),
                'status' => 'init',
            ]);

            $room->save();

            return redirect('/init-room/' . $room->id);
        }
    }

    //Inicia el juego
    public function initRoom($roomID)
    {
        $room = Room::find($roomID);    

        if($room["status"] == 'init')
        {
            $room["status"] = "playing";
            $room->save();

            return view('juego', compact('room'));
        }
        else
            return redirect('');
    }

    //Cierra el juego
    public function closeRoom(Request $request)
    {
        $room = Room::find($request->get('roomID'));    

        if($room["status"] == 'playing')
        {
            $room["status"] = "";
            $room->save();
        }

        return redirect('');
    }

    //Ingresa a una sala que no este siendo utilizada
    public function enterRoom(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'roomID' => 'required|string|min:1|max:80'
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)
                        ->withInput();
        }
        else
        {
            $room = Room::find($request->get('roomID'));

            if($room["status"] == '')
            {
                $room["status"] = "init";
                $room->save();

                return redirect('/init-room/' . $room->id);
            }
            else
            {
                return back()->withErrors($validator)
                        ->withInput();
            }
        }
    }

}
