@extends('layouts.app')

@section('content')
<div class="d-flex align-items-center justify-content-center myCenter">
    <div class="row">
        <a class="btn btn-primary btn-lg btn-block" href="{{ url('nueva-partida') }}" role="button">Nueva partida</a>
        <a class="btn btn-primary btn-lg btn-block mt-5" href="{{ url('unirse-partida') }}" role="button">Unirse a una partida</a>
    </div>

</div>

@endsection
