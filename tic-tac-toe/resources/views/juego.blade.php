@extends('layouts.app')

@section('content')
<div class="d-flex align-items-center justify-content-center myCenter">
    <div class="container">
    	<div class="col-md-12">
    		<span class="textoJuego">El ID de esta sala es {{ $room["id"]}}</span>
    		<span class="textoJuego" id="turnoActual" data-player1="{{ $room['player1']}}" data-player2="{{ $room['player2']}}">El turno actual es de {{ $room['player1']}}</span>
	        <div class="col-md-12">
	            <div class="text-vertical-center">
	                <div class="row">
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="0" data-y="0">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="1" data-y="0">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="2" data-y="0">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="0" data-y="1">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="1" data-y="1">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="2" data-y="1">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="0" data-y="2">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="1" data-y="2">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="bloque" data-x="2" data-y="2">
	                        	<div class="text-center simbolos"></div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
        </div>

        <div class="col-md-12">
            <div class="text-center text-vertical-center">
                <div class="row">

					<div class="col-md-3">
					</div>

					<div class="col-md-3">
						<button id="resetButton" class="btn btn-primary btn-lg btn-block">Reiniciar partida</button>
					</div>

					<div class="col-md-3">
		                <form method="POST" action="{{ route('close-room') }}">
		                	@csrf
							<input name="roomID" type="hidden" value='{{$room["id"]}}'>
		
							<button id="closeJuego" class="btn btn-primary btn-lg btn-block">Terminar partida</button>
		                </form>
					</div>

					<div class="col-md-3">
					</div>
				</div>
			</div>
		</div>

    </div>

</div>

@endsection


