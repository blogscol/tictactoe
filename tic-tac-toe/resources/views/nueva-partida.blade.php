@extends('layouts.app')

@section('content')
<div class="d-flex align-items-center justify-content-center myCenter">
    <div class="row">

        <div class="card">
            <div class="card-header">Ingresar nombre</div>

            <div class="card-body">
                <form method="POST" action="{{ route('new-room') }}">
                	@csrf
                    <div class="form-group row">
						<input id="player1" type="text" class="form-control @error('player1') is-invalid @enderror" name="player1" value="Player 1">

                        <input id="player2" type="text" class="form-control @error('player2') is-invalid @enderror" name="player2" value="Player 2">

						<button class="btn btn-primary btn-lg btn-block">Nueva partida</a>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>

@endsection


