@extends('layouts.app')

@section('content')
<div class="d-flex align-items-center justify-content-center myCenter">
    <div class="row">

        <div class="card">
            <div class="card-header">Ingresar nombre</div>

            <div class="card-body">
                <form method="POST" action="{{ route('enter-room') }}">
                	@csrf
                    <div class="form-group row">
                        
                        <input id="roomID" type="text" class="form-control @error('roomID') is-invalid @enderror" name="roomID" placeholder="Ingresa el ID del la sala">

						<button class="btn btn-primary btn-lg btn-block">Unirse a una partida</a>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>

@endsection


