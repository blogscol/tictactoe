var tablero =
[
    ["", "", ""],
    ["", "", ""],
    ["", "", ""]
];

var playerActual = 0;

var player1 = "X";
var player2 = "O";

var turnosActual = 0;
var playerNombre;

var hayGanador = false;

$('#resetButton').hide();
$('#closeJuego').hide();

$('#resetButton').click(function()
{
    reiniciarTablero();
});

$(".bloque").click(function()
{
    var Coord_X = parseInt($(this).data('x'));
    var Coord_Y = parseInt($(this).data('y'));
    
    if(tablero[Coord_Y][Coord_X] == "" && hayGanador == false)
    {
        var playerSimbolo;

        if(playerActual == 0)
            playerSimbolo = player1;
        else
            playerSimbolo = player2;

        tablero[Coord_Y][Coord_X] = playerSimbolo;

        var targetDiv = $(this).children("div").text(playerSimbolo);

        revisarTablero();
    }
});

function reiniciarTablero()
{
    $('#resetButton').hide();
    $('#closeJuego').hide();

    for (var i = 0; i < tablero.length; i++)
    {
        tablero[0][i] = "";
        tablero[1][i] = "";
        tablero[2][i] = "";
    }

    $(".simbolos").html('');

    if(turnosActual < 9)
    {
        if(playerActual == 1)
            playerActual = 0;
        else
            playerActual = 1;
    }

    turnosActual = 0;
    hayGanador = false;

    if(playerActual == 1)
    {
        playerNombre = $('#turnoActual').data('player2');
    }
    else
    {
        playerNombre = $('#turnoActual').data('player1');
    }

    $('#turnoActual').html('El turno actual es de ' + playerNombre);
}

function revisarVertical()
{
    for (var i = 0; i < tablero.length; i++)
    {
        if(tablero[0][i] != "")
        {
            if(tablero[0][i] == tablero[1][i] && tablero[0][i] == tablero[2][i])
                return true;
        }
    }

    return false;
}

function revisarHorizontal()
{
    for (var i = 0; i < tablero.length; i++)
    {
        if(tablero[i][0] != "")
        {
            if(tablero[i][0] == tablero[i][1] && tablero[i][0] == tablero[i][2])
                return true;
        }
    }

    return false;
}

function revisarDiagonal()
{
    if(tablero[0][0] != "")
    {
        if(tablero[0][0] == tablero[1][1] && tablero[0][0] == tablero[2][2])
            return true;
    }

    if(tablero[0][2] != "")
    {
        if(tablero[0][2] == tablero[1][1] && tablero[0][2] == tablero[2][0])
            return true;
    }

    return false;
}

function revisarTablero()
{
    if(revisarHorizontal() == true || revisarVertical() == true || revisarDiagonal() == true)
    {
        hayGanador = true;
        $('#resetButton').show();
        $('#closeJuego').show();

    }
    else
    {
        if(playerActual == 0)
        {
            playerActual = 1;
            playerNombre = $('#turnoActual').data('player2');
        }
        else
        {
            playerActual = 0;
            playerNombre = $('#turnoActual').data('player1');
        }

        turnosActual++;

        if(turnosActual == 9)
        {
            $('#resetButton').show();
            $('#closeJuego').show();
        }
        else
        {         
            $('#turnoActual').html('El turno actual es de ' + playerNombre);
        }
    }
}